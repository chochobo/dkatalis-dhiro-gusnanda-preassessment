import java.text.SimpleDateFormat;
import java.util.Date;

public class SpringBootProblem15 {
    //type your code here
	public static void main( String[] args ) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
		Date d1 = sdf.parse("2018-09-25T02:10:24");
		Date d2 = sdf.parse("2019-08-25T02:10:24");
		
		long diffInMillies = Math.abs(d2.getTime() - d1.getTime());
	    long result = diffInMillies/1000;
		
	    System.out.println( diff );
	}
}
