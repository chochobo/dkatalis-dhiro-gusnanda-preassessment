import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SpringBootProblem15 {
    //type your code here
	public static void main( String[] args ) {
    	int n = 20;
    	
    	int sum = 0;
    	for(int i=2 ; i <= n; i++) {
    		if (isPrime(i)) {
				sum += i;
			}
    	}
    	System.out.println(sum);
    }
    
    public static boolean isPrime(int n) {
        for(int i=2;2*i<n;i++) {
            if(n%i==0)
                return false;
        }
        return true;
    }
}
